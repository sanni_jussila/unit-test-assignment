const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib')

describe('Unit testing mylib.js', () => {

    let beforeVar = undefined;
    before(() => {
        beforeVar = 1;
    })
    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2);
    })
    it('beforeVar exists', () => {
        should.exist(beforeVar);
    })
    after(() => {
        console.log('I am after test');
    })
})