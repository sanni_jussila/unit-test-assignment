const express = require('express')
const app = express()
const port = 3000
const mylib = require('./mylib')

//endpoint localhost:3000/
app.get('/', (req, res) => {
  res.send('This is unit-test assignment!')
})

//endpoint localhost:3000/add?a=42&b=21    
app.get('/add', (req, res) =>  {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    res.send(mylib.sum(a,b).toString());
})

app.listen(port, () => {
  console.log(`Server: http://localhost:${port}`);
})